#include <stdio.h>
#include <assert.h>
#include "mimetypes.h"

int
main (int argc, char **argv)
{
  int i;
  char const *type;
  assert (argc >= 3);
  assert (mimetypes_parse (argv[1]) == 0);
  for (i = 2; i < argc; i++)
    {
      printf ("%s:", argv[i]);
      type = get_file_type (argv[i]);
      if (type)
	printf (" %s", type);
      putchar ('\n');
    }
  return 0;
}

  
