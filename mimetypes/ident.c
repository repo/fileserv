/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdlib.h>
#include <errno.h>
#include <string.h>

struct ident_ref
{
  char *name;
  size_t count;
  struct ident_ref *next;
};

static struct ident_ref *nametab;

int
ident_ref (char const *name, char const **refname)
{
  struct ident_ref *ref = NULL;
  
  if (!refname)
    return EINVAL;

  if (!name)
    {
      *refname = NULL;
      return 0;
    }
  
  if (nametab)
    {
      for (ref = nametab; ref; ref = ref->next)
	{
	  if (strcmp (ref->name, name) == 0)
	    break;
	}
    }

  if (!ref)
    {
      ref = malloc (sizeof (*ref));
      if (!ref)
	return -1;
      ref->name = strdup (name);
      if (!ref->name)
	{
	  free (ref);
	  return -1;
	}
      ref->count = 0;
      ref->next = nametab;
      nametab = ref;
    }
  
  ref->count++; 
  *refname = ref->name;
 
  return 0;
}

void
ident_deref (char const *name)
{
  struct ident_ref *ref, *prev;

  if (!name || !nametab)
    return;

  for (ref = nametab, prev = NULL; ref; prev = ref, ref = ref->next)
    {
      if (strcmp (ref->name, name) == 0)
	{
	  if (--ref->count == 0)
	    {
	      if (prev)
		prev->next = ref->next;
	      else
		nametab = ref->next;
	      free (ref->name);
	      free (ref);
	    }
	  break;
	}
    }
}
  

