/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <errno.h>
#include "locus.h"

int
locus_point_set_file (struct locus_point *pt, const char *filename)
{
  int rc;
  char const *ref;

  rc = ident_ref (filename, &ref);
  if (rc)
    return rc;
  ident_deref (pt->file);
  pt->file = ref;
  return 0;
}

void
locus_point_init (struct locus_point *pt)
{
  memset (pt, 0, sizeof *pt);
}

void
locus_point_deinit (struct locus_point *pt)
{
  ident_deref (pt->file);
  memset (pt, 0, sizeof *pt);
}

int
locus_point_copy (struct locus_point *dest, struct locus_point const *src)
{
  int rc = locus_point_set_file (dest, src->file);
  if (rc == 0)
    {
      dest->col = src->col;
      dest->line = src->line;
    }
  return rc;
}

void
locus_range_init (struct locus_range *dest)
{
  memset (dest, 0, sizeof *dest);
}

int
locus_range_copy (struct locus_range *dest, struct locus_range const *src)
{
  int rc;
  struct locus_range tmp = LOCUS_RANGE_INITIALIZER;

  if (!dest)
    return EINVAL;
    
  rc = locus_point_copy (&tmp.beg, &src->beg);
  if (rc == 0)
    {
      rc = locus_point_copy (&tmp.end, &src->end);
      if (rc)
	locus_point_deinit (&tmp.beg);
      else
	{
	  locus_range_deinit (dest);
	  *dest = tmp;
	}
    }
  return rc;
}

void
locus_range_deinit (struct locus_range *lr)
{
  locus_point_deinit (&lr->beg);
  locus_point_deinit (&lr->end);
}
