/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include "fileserv.h"
#include <string.h>
#include <errno.h>
#include <signal.h>

char *pidfile;

void
pidfile_remove(void)
{
	if (pidfile && unlink(pidfile))
		error("cannot unlink pidfile `%s': %s",
		      pidfile, strerror(errno));
}

void
pidfile_create(void)
{
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "w");
	if (!fp) {
		error("cannot create pidfile `%s': %s",
		      pidfile, strerror(errno));
		exit(1);
	}
	fprintf(fp, "%lu", (unsigned long) getpid());
	fclose(fp);
}

/* Check whether pidfile exists and if so, whether its PID is still
   active. Exit if it is. */
void
pidfile_check(void)
{
	unsigned long pid;
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "r");

	if (fp) {
		if (fscanf(fp, "%lu", &pid) != 1) {
			error("cannot get pid from pidfile `%s'", pidfile);
		} else {
			if (kill(pid, 0) == 0) {
				error("%s appears to run with pid %lu. "
				      "If it does not, remove `%s' and retry.",
				      progname, pid, pidfile);
				exit(1);
			}
		}

		fclose(fp);
		if (unlink(pidfile)) {
			error("cannot unlink pidfile `%s': %s",
			      pidfile, strerror(errno));
			exit(1);
		}
	} else if (errno != ENOENT) {
		error("cannot open pidfile `%s': %s",
		      pidfile, strerror(errno));
		exit(1);
	}
}
