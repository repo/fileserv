/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <string.h>
#include "fileserv.h"

char *
catfile_suf_n(char const *dir, size_t len, char const *file, char const *suf)
{
	size_t flen, slen;
	char *res;
	
	while (len > 0 && dir[len-1] == '/')
		len--;
	while (*file == '/')
		file++;
	flen = strlen(file);
	while (flen && file[flen-1] == '/')
		--flen;
	slen = suf ? strlen(suf) : 0;
	res = malloc(len + flen + slen + 2);
	if (res) {
		char *p;
		
		memcpy(res, dir, len);
		p = res + len;
		if (flen) {
			*p++ = '/';
			memcpy(p, file, flen);
			p += flen;
		}
		if (slen) {
			memcpy(p, suf, slen);
			p += slen;
		}
		*p = 0;
	} else
		alloc_warn();
	return res;
}

char *
catfile_n(char const *dir, size_t len, char const *file)
{
	return catfile_suf_n(dir, len, file, NULL);
}

char *
catfile_suf(char const *dir, char const *file, char const *suf)
{
	return catfile_suf_n(dir, strlen(dir), file, suf);
}

char *
catfile(char const *dir, char const *file)
{
	return catfile_n(dir, strlen(dir), file);
}
