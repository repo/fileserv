/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include "fileserv.h"
#include <string.h>
#include <syslog.h>

static int facility = LOG_DAEMON;

static void
vlog_syslog(int prio, char const *fmt, va_list ap)
{
	vsyslog(prio, fmt, ap);
}

static void
vlog_stderr(int prio, char const *fmt, va_list ap)
{
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

static void (*vlog)(int prio, char const *fmt, va_list ap) = vlog_stderr;

void
error(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vlog(LOG_ERR, fmt, ap);
	va_end(ap);
}

void
info(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vlog(LOG_INFO, fmt, ap);
	va_end(ap);
}

void
fileserv_logger(void *arg, const char *fmt, va_list ap)
{
	vlog(LOG_ERR, fmt, ap);
}

void
syslog_enable(void)
{
	openlog(progname, LOG_PID, facility);
	vlog = vlog_syslog;
}

static struct facility {
	char const *s;
	int n;
} facilities[] = {
	{ "auth", LOG_AUTH },
	{ "authpriv", LOG_AUTHPRIV },
	{ "cron", LOG_CRON },
	{ "daemon", LOG_DAEMON },
	{ "ftp", LOG_FTP },
	{ "kern", LOG_KERN },
	{ "lpr", LOG_LPR },
	{ "mail", LOG_MAIL },
	{ "news", LOG_NEWS },
#ifdef LOG_AUTH
	{ "security", LOG_AUTH },
#endif
	{ "syslog", LOG_SYSLOG },
	{ "user", LOG_USER },
	{ "uucp", LOG_UUCP },
	{ "local0", LOG_LOCAL0 },
	{ "local1", LOG_LOCAL1 },
	{ "local2", LOG_LOCAL2 },
	{ "local3", LOG_LOCAL3 },
	{ "local4", LOG_LOCAL4 },
	{ "local5", LOG_LOCAL5 },
	{ "local6", LOG_LOCAL6 },
	{ "local7", LOG_LOCAL7 },
	{ NULL, -1 }
};
	  
int
set_log_facility(char const *arg)
{
	struct facility *f;
	for (f = facilities; f->s; f++)
		if (strcmp(f->s, arg) == 0)
			break;
	if (f->n == -1)
		return -1;
	facility = f->n;
	return 0;
}

