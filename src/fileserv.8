.\" This file is part of fileserv -*- nroff -*-
.\" Copyright (C) 2017-2024 Sergey Poznyakoff
.\"
.\" Fileserv is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Fileserv is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with fileserv.  If not, see <http://www.gnu.org/licenses/>.
.TH FILESERV 8 "February 16, 2023" "FILESERV" "User Commands"
.SH NAME
fileserv \- simple http server for static files
.SH SYNOPSIS
\fBfileserv\fR\
 [\fB\-fhsv\fR]\
 [\fB\-a\fR [\fIIP\fR][\fB:\fIPORT\fR]]\
 [\fB\-c\fR \fIFILE\fR]\
 [\fB\-g\fR \fIGROUP\fR]\
 [\fB\-u\fR \fIUSER\fR]\
 [\fIHOST\fB:\fR]\fIURL\fB:\fIDIR\fR ...
.SH DESCRIPTION
A simple and lightweight HTTP server. It serves static files from one
or more directories on the file system. The directories to be used and
the corresponding URLs are defined in the form of mappings, either in
the command line or in the configuration file. The mapping
.PP
.EX
  \fIPATH\fR:\fIDIR\fR
.EE
.PP
instructs the server to map all URLs that begin with the path prefix
\fIPATH\fR to the directory \fIDIR\fR on the file system. The mapping
.PP
.EX
  \fIHOST\fR:\fIPATH\fR:\fIDIR\fR
.EE
.PP
has the same effect, but only if the \fBHost:\fR header of the
incoming HTTP request has the value \fIHOST\fR.
.PP
Depending on the configuration settings, the directory listing can be
produced.
.SH OPTIONS
.TP
\fB\-a [\fIIP\fR][\fB:\fIPORT\fR]
Listen on the given IP address and port. Either \fIIP\fR or \fIPORT\fR (but
not both) can be omitted. If \fIIP\fR is omitted, the colon must be
retained.
.sp
This option overrides the \fBlisten\fRv configuration setting.
.TP
\fB\-c \fIFILE\fB
Read configuration from \fIFILE\fR, instead of \fB/etc/fileserv.conf\fR.
.TP
.B \-f
Run in foreground.
.TP
.BI \-g " GROUP"
Run with this primary group privileges. \fIGROUP\fR must be either the
name of a group listed in the user database, or a numeric GID prefixed
with a plus sign.
.sp
This option overrides the \fBgroup\fR configuration setting.
.TP
.B \-h
Display a terse command line summary and exit.
.TP
.B \-s
Run in single-process mode. In this mode, \fBfileserv\fR does not
start a sentinel process.
.TP
.BI \-u " USER"
Run with this user privileges. \fIUSER\fR must be either the
name of a user listed in the system user database, or a numeric UID
prefixed with a plus sign.
.sp
This option overrides the \fBuser\fR configuration setting.
.TP
.B \-v
Increase verbosity level. Multiple options accumulate.
.TP
.B \-V
Show program version, license and copyright information.
.SH CONFIGURATION FILE
The server reads most of its settings from the configuration file
\fB/etc/fileserv.conf\fR (the default location can be overridden using
the \fB\-c\fR command line option). The file has a traditional UNIX
configuration file syntax. Each statement occupies a single line and
consists of configuration keyword followed by one or more
values, separated by whitespace characters. Values containing
whitespace characters should be quoted (with either single or double
quote characters). Backslash character serves to escape the following
character. Usual \fBC\fR escapes are understood. Empty lines are
ignored. Comments are introduced by \fB#\fR symbol. 
.PP
.TP
\fBuser\fR \fINAME\fR
Run as the user \fINAME\fR. Argument must be either login name of a
user listed in the user database, or a numeric UID prefixed with a
plus sign.

Notice, that in single-process mode (see the \fB\-s\fR option)
switching to user privileges is performed before writing the PID file,
so make sure that the directory it is located in is writable for this
user (see the \fBpidfile\fR statement).
.TP
\fBgroup\fR \fINAME\fR
Run with primary group \fINAME\fR. Argument must be either the name of
a group listed in the user database, or a numeric GID prefixed with a
plus sign. By default, the primary group of the user supplied with
the \fBuser\fR statement will be used.

Note, that supplementary groups are always honored.
.TP
\fBpidfile\fR \fBFILE\fR
Save PID of the running process to \fIFILE\fR. See the notice to the
\fBuser\fR statement, above.

For compatibility with earlier versions, the keyword can also be
spelled as \fBpid\-file\fR.
.TP
\fBmime\-types\-file\fR \fIFILE\fR
Name of the MIME magic file. This file is used to determine the
types of the files being served or listed. The file syntax is the same
as for \fBcups\fR. See
.BR mime.types (5),
for a detailed discussion. The \fBapache\fR's mime types file, which is a
simplification of this format, can also be used.
.TP
\fBlisten\fR \fIIPADDR\fB:\fIPORT\fR
Listen on the given IP address and port. Either IPADDR or PORT (but
not both) can be omitted. If IPADDR is omitted, the colon must be
retained.

Notice, that multiple listen addresses are not supported.

Default address is \fB0.0.0.0:8080\fR.
.TP
\fBaccess\-file\-name\fR \fINAME\fR
Name of the per-directory access file. The file has the same syntax as
the main configuration file, except that only the following statements
are allowed:
.BR directory\-index ,
.BR follow , 
.BR listing ,
.BR list\-unreadable ", and"
.BR hidden-files .

Default access file name is
.BR .fileserv .
.TP
\fBforwarded\-header\fR \fINAME\fR
Treat the value of the HTTP header \fINAME\fR as a comma-delimited list of
intermediate useragent IP addresses. To determine the actual useragent
IP, the list is traversed from right to left, until an IP is found
that is not listed as trusted-proxy address (see below).

Default header name is
.BR X\-Forwarded\-For .
.TP
\fBtrusted\-proxy\fR \fBIP\fR [\fBIP\fR...]
List of IP addresses of trusted proxies. One or more IP addresses can
be given. Multiple statements accumulate. The initial list is empty.
.TP
\fBtemp\-dir\fR \fIDIR\fR
Store temporary files in \fIDIR\fR. Default is \fB/tmp\fR.
.TP
\fBsyslog\fR \fINAME\fR
Syslog facility name to use for reporting. Valid values for \fINAME\fR
are:
.BR auth ,
.BR authpriv ,
.BR cron ,
.BR daemon " (default), "
.BR ftp ,
.BR kern ,
.BR lpr ,
.BR mail ,
.BR news ,
.BR security ,
.BR syslog ,
.BR user ,
.BR uucp ,
and
.B local0
through
.BR local7. 
.SS Access control settings
The statements discussed below configure access control
settings. These remain in effect for all subdirectories of the
selected mapping, until overridden by the corresponding statements in
the per-directory access files (see \fBaccess\-file\-name\fR, above).
.PP
Some of these statements take a boolean value (denoted as \fIBOOL\fR)
as their arguments. Valid boolean values are (case-insensitive):
.BR 1,
.BR yes ,
.BR true ,
or
.BR on,
indicating true value, and
.BR 0 ,
.BR no ,
.BR false ,
or
.BR off,
indicating false value.
.TP
\fBdirectory\-index\fR \fIFILE\fR [\fIFILE\fR...]
Directory index file names. One or more filename arguments are
expected. If the first argument is \fB+\fR (a plus sign), all subsequent
arguments are appended to the current list, instead of overwriting
it. This is for use in per-directory access files.
.TP
\fBfollow\fR \fIBOOL\fR
Follow symbolic links. If set to \fByes\fR, symbolic links appearing
in the request will be resolved. Notice, that the resulting file or
directory will be served only if it is located in one of the
configured mappings.

Default is \fBno\fR.
.TP
\fBlisting\fR \fIBOOL\fR
Enable directory listing mode. Directory listing will be returned as a
response for a request with URL mapping to a directory without valid
index file. The format of the listing file is configurable. See the
section \fBLISTING TEMPLATE\fR, for a detailed discussion.

Default is \fBno\fR.
.TP
\fBlist\-unreadable\fR \fIBOOL\fR
Whether to include in the listing the names of files that cannot be
read.

Default is \fBno\fR.
.TP
\fBhidden\-files\fR \fIREGEX\fR [\fIREGEX\fR...]
When producing a directory listing, omit any files whose names match
one of the POSIX extended regular expressions from this list.
.SS Mapping
Mappings declare correspondence between URLs of incoming requests
and directories on the local filesystem. At least one mapping must
be defined.
.TP
\fBmapping\fR [\fIHOST\fB:\fR]\fIURL\fB:\fIDIR\fR
HTTP requests with path beginning with \fIURL\fR will be mapped to
local file paths beginning with \fIDIR\fR. Optional \fIHOST\fR part
limits the mapping to requests that have this value in the HTTP
"Host:" header. This is similar to Apache's \fBAlias\fR directive.
.SS Error documents
.TP
\fBerror\-dir\fR \fIDIR\fR
Look for customized versions of error pages in the directory \fIDIR\fR.
specified by the \fBerror\-dir statement.
.PP
When reporting a HTTP error, \fBfileserv\fR will search in that directory
for the first existing file from the following list:
\fICODE\fB.html\fR,
\fICODE\fB.html.var\fR,
\fIERR\fB.html,\fR,
\fIERR\fB.html.var\fR,
where \fICODE\fR is the HTTP error code, and \fIERR\fR is its internal name.
Allowed \fICODE\fRs and the corresponding \fIERR\fR names are:
403 (HTTP_FORBIDDEN), 404 (HTTP_NOT_FOUND), 405 (HTTP_METHOD_NOT_ALLOWED),
500 (HTTP_INTERNAL_SERVER_ERROR), 501 (HTTP_NOT_IMPLEMENTED), and
503 (HTTP_SERVICE_UNAVAILABLE).
.PP
If the file with \fB.html\fR suffix is found, it will be served as the body of
the error response. If the file with \fB.html.var\fR suffix is found, it
will be treated as a type map in Apache's \fBmod_negotiation\fR.
A type map has a format similar to RFC822 mail headers. It contains
error document descriptions separated by blank lines, with lines beginning
with a hash character treated as comments. A document description
consists of several header records; records may be continued on multiple
lines if the continuation lines start with spaces. The leading space will be
deleted and the lines concatenated. A header record consists of a keyword
name, which always ends in a colon, followed by a value. Whitespace is
allowed between the header name and value, and between the tokens of value.
The headers allowed are:
.TP
.B Content\-Language:
The language(s) of the variant, as an Internet standard language tag
(RFC 1766), e.g. en, meaning English. If the variant contains more than
one language, they are separated by a comma.

This header is a look-up key.
.TP
.B Content\-Type:
The MIME media type of the document, with optional parameters.
.TP
.B Body:
The actual content of the error document is included in the type-map
file using the \fBBody\fR header. This header must contain a string that
designates a delimiter for the body content. Then all following lines
in the type map file will be considered part of the document body until
the delimiter string is found. 
.PP
The server will select this error description whose Content-language header
matches the language definitions from the \fBAccept\-Language\fR
request header.
.SS Listing template
The following statements control generation of directory index
listings (see the \fBlisting\fR statement, above).
.TP
\fBindex\-template\fR \fIFILE\fR
By default, \fBfileserv\fR uses the built-in template (see
the file \fBsrc/defidx.html\fR). This statement instructs it to use
\fIFILE\fR instead. See the section \fBLISTING TEMPLATE FILE\fR, for a
discussion of its syntax. \fIFILE\fR must be an absolute file name.
.TP
\fBindex\-css\fR \fIFILE\fR
Name of the CSS file to use in listings. It will be available in the
\fB$INDEXCSS\fR template variable. \fIFILE\fR must be an absolute file
name.
.TP
\fBmime\-icon\fR \fIURL\fR [\fBalt=\fITEXT\fR] \fITYPE\fR [\fITYPE\fR...]
Defines icon to use for files of given MIME types. Each \fITYPE\fR is
treated as globbing pattern (see \fBglob\fR(7)). \fIURL\fR is the
URL of the image file to be displayed for matching files, and \fITEXT\fR
is the alternative text.
.TP
\fBname\-icon\fR \fIURL\fR [\fBalt=\fITEXT\fR] \fISUFFIX\fR [\fISUFFIX\fR...]
Defines icon for to use file names that end in one of the \fISUFFIX\fRes. See
\fBmime\-icon\fR, for the description of \fIURL\fR and \fITYPE\fR.
.TP
\fBtype\-icon\fR \fIURL\fR [\fBalt=\fITEXT\fR] \fITYPE\fR [\fITYPE\fR...]
Defines icon to use for file of the given type. Available \fITYPE\fRs
are: \fBDIRECTORY\fR, for a directory, \fBFILE\fR, for a regular file,
and \fBBLANK\fR, for a blank line.
.SH LISTING TEMPLATE
Listing template file is used to produce directory listings. It is a
normal HTML file, except for the following special constructs:
.TP
.BI "{% " STRING " %}"
Replaced with the expansion of \fISTRSING\fR
.TP
.BI "{% if " STRING " %}"
Starts conditional construct. The syntax is:
.EX
{% if \fISTRING\fR %}
.I "  TEXT-IF-TRUE"
{% else %}
.I "  TEXT-IF-FALSE"
{% endif %}
.EE
If the expansion if \fISTRING\fR is not empty, the construct is
replaced with \fITEXT-IF-TRUE\fR, otherwise it is replaced with
\fITEXT-IF-FALSE\fR. The else branch is optional.
.TP
.B {% loop %}
Starts main loop. The syntax is:

.EX
{% loop %}
.I "  TEXT"
{% endloop %}
.EE

For each file found in the directory being listed, the loop construct
produces expansion of \fITEXT\fR.
.PP
The \fIexpansions\fR performed on strings in template constructs are:
.SS Variable expansion
Variable expansion replaces each occurrence of
.BI $ NAME
or
.BI ${ NAME }
with the value of the templave variable \fINAME\fR.
.PP
The forms below test for a variable that is unset or null.  Omitting the
colon results in a test only for a variable that is unset.
.TP
.BI ${ variable :- word }
.BR "Use Default Values" .
If \fIvariable\fR is unset or null, the expansion of \fIword\fR is substituted.
Otherwise, the value of \fIvariable\fR is substituted.
.TP
.BI ${ variable := word }
.BR "Assign Default Values" .
If \fIvariable\fR is unset or null, the expansion of \fIword\fR is
assigned to \fIvariable\fR.  The value of \fIvariable\fR is then substituted.
.TP
.BI ${ variable :? word }
.BR "Display Error if Null or Unset" .
If \fIvariable\fR is null or unset, the expansion of \fIword\fR (or a
message to that effect if word is not present) is sent to the
program error output stream. Otherwise, the value of \fIvariable\fR is
substituted.
.TP
.BI ${ variable :+ word }
.BR "Use Alternate Value" .
If \fIvariable\fR is null or unset, nothing is substituted, otherwise the
expansion of \fIword\fR is substituted.
.PP
The following template variables are defined:
.TP
.B URI
URL of the object being listed.
.TP
.B INDEXCSS
The value of \fBindex\-css\fR configuration statement.
.PP
The following variables are available only within the loop construct:
.TP
.B ROWCLASS
Expands to the string \fBodd\fR or \fBeven\fR, depending on the number
of iteration within loop.
.TP
.B FILENAME
Current file name.
.TP
.B FILESIZE
Current file size.
.TP
.B FILETIME
Last modification time of the current file, in format "%Y-%m-%d %H:%M".
.TP
.B FILETYPE
Type of the current file: either \fBDIRECTORY\fR or \fBFILE\fR.
.TP
.B MIMETYPE
MIME type of the current file, as determined using the
\fBmime\-types\-file\fR.
.SS Command substitution
During \fIcommand substitution\fR, each word is scanned for commands.
Each command found is executed and replaced with the output it creates.
.PP
The syntax is:
.PP
.RS +4
.BI $( command )
.RE
.PP
Command substitutions may be nested.
.PP
Available template commands are:
.TP
.BI iconlookup " NAME MIME TYPE"
Look up the icon to use for file \fINAME\fR, using the list of
\fBname\-icon\fR statements. If no icon is found and the MIME type
\fIMIME\fR is given, look up the icon for that MIME type, using the
\fBmime\-icon\fR statements. If the icon file is not found after that
step, and the file type \fITYPE\fR is given, then look up the icon for
that type, using the \fBtype\-icon\fR configuration statements. On
success, return the \fIURL\fR of the icon. If no matching icon was found,
return empty string.
.TP
.B iconsrc
If the previous call to
.B $(iconlookup)
succeeded, returns the URL of the icon.
.TP
.B iconalt
If the previous call to
.B $(iconlookup)
succeeded, returns the alternative text for the icon, i.e. the
\fITEXT\fR part of the \fBalt=\fR parameter to the matching
\fBname\-icon\fR, \fBmime\-icon\fR, or \fBtype\-icon\fR statement.
.TP
.BI updir " URL"
Returns the URL obtained by removing the last firectory component from
the \fIURL\fR argument, or empty string if the latter is \fB/\fR.
.TP
.BI sortorder " COL"
Reverts the current sort order for the sorting column
\fICOL\fR. Possible values for the argument are:
.sp
.nf
.ta 8n 20n
.ul
	N	File name
	M	File modification date
	S	File size
	D	Description
.fi
.sp
Return value is \fBA\fR for ascending, and \fBD\fR for descending
order.
.sp
This function is useful to create listing ordering menu, e.g.:
.sp
.EX
     <th>
       <a href="?C=N&amp;O={% $(sortorder N) %}">Name</a>
     </th>
     <th>
       <a href="?C=M&amp;O={% $(sortorder M) %}">Last modified</a>
     </th>
     <th>
       <a href="?C=S&amp;O={% $(sortorder S) %}">Size</a>
     </th>
.EE
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.       ds package fileserv
.       ds version 0.5
.       so download.inc
\}
.SH COPYRIGHT
Copyright \(co 2017--2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
