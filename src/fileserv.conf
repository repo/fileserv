# Run as this user.
# Argument must be either login name of a user listed in the user database,
# or a numeric UID prefixed with a plus sign.
#user root

# Run as this primary group.
# Argument must be either the name of a group listed in the user database,
# or a numeric GID prefixed with a plus sign.
# By default, the primary group of the user supplied with the 'user' statement
# will be used.
# Note that supplementary groups are always honored.
#group root

# Name of the PID file
# Notice, that the file is created after assuming user privileges, so
# you should make sure that its directory is writable for the user
# fileserv is running as.
pidfile /var/run/fileserv.pid

# Name of the MIME magic file. This file is used to determine the
# types of the files being served or listed. E.g.:
#
#  mime-types-file /etc/fileserv/mime.types
#
# Default is undefined

# Listen on the given interface 
#listen 0.0.0.0:8080

# Name of the per-directory access file.
# The file has the same syntax as /etc/fileserv.conf, except that only
# the following statements are allowed: directory-index, follow,
# listing, list-unreadable, and hidden-files.
#access-file-name .fileserv

# Treat the value of the specified header as a comma-delimited list of
# intermediate useragent IP addresses. To determine the actual useragent
# IP, the list is traversed from right to left, until an IP is found
# that is not listed as trusted-proxy address (see below).
#
#forwarded-header X-Forwarded-For

# List of IP addresses of trusted proxies. Expects one or more arguments.
# Multiple statements accumulate. Initial list is empty.
trusted-proxy 127.0.0.1

# Directory for temporary files.
#temp-dir /tmp

# Syslog facility to use for reporting.
#syslog daemon

# Global access control settings. These remain in effect for all
# subdirectories of the selected mapping, until overridden by the
# corresponding statements in the per-directory access files (see
# access-file-name above).

# Directory index file names. One or more filename arguments are
# expected. If the first argument is + (a plus sign), subsequent
# arguments are appended to the current list, instead of overwriting
# it. This is for use in per-directory access files.
directory-index index.html

# Follow symbolic links.
# If set to "yes", symbolic links appearing in the request will be
# resolved. Notice, that the resulting file or directory will be served
# only if it is located in one of the configured mappings.
#follow no

# Enable directory listing mode.
#listing no

# Add unreadable files to the listing.
#list-unreadable no

# When producing a directory listing, omit any files whose names match
# one of the POSIX extended regular expressions from this list.
hidden-files '^\..*' '^#' '~$' '\.rej$' '\.orig$'

# MAPPINGS
# Mappings define which directories fileserv will be serving.
#
# Each statement declares a mapping between the URL of the incoming request
# and the directory on the local filesystem (think of Apache Alias directive).
# The syntax is
#    [HOST:]URL:DIR
# URLs with a path beginning with URL will be mapped to local file paths
# beginning with DIR. Optional HOST part limits the mapping to requests that
# have this value in the HTTP Host: header.
#
# At least one mapping must be defined.

mapping /:/var/www/htdocs
mapping /.well-known/acme-challenge:/var/www/acme/

# ERROR DOCUMENTS
# Customized versions of error pages are looked up in the directory,
# specified by the error-dir statement.
# When reporting a HTTP error, fileserv will search in that directory
# for the first existing file from the following list:
#       CODE.html, CODE.html.var, ERR.html, ERR.html.var
# where CODE is the HTTP error code, and ERR is its internal name.
# Allowed CODEs and the corresponding ERR names are:
#
#    403 HTTP_FORBIDDEN
#    404 HTTP_NOT_FOUND
#    405 HTTP_METHOD_NOT_ALLOWED
#    500 HTTP_INTERNAL_SERVER_ERROR
#    501 HTTP_NOT_IMPLEMENTED
#    503 HTTP_SERVICE_UNAVAILABLE
#
# If the file with .html suffix is found, it will be served as the body of
# the error response. If the file with .html.var suffix is found, it
# will be treated as a type map in Apache's mod_negotiation:
# A type map has a format similar to RFC822 mail headers. It contains
# error document descriptions separated by blank lines, with lines beginning
# with a hash character ('#') treated as comments. A document description
# consists of several header records; records may be continued on multiple
# lines if the continuation lines start with spaces. The leading space will be
# deleted and the lines concatenated. A header record consists of a keyword
# name, which always ends in a colon, followed by a value. Whitespace is
# allowed between the header name and value, and between the tokens of value.
# The headers allowed are:
#  Content-Language:
#     The language(s) of the variant, as an Internet standard language tag
#     (RFC 1766), e.g. en, meaning English. If the variant contains more than
#     one language, they are separated by a comma.
#     This header is a look-up key
#  Content-Type:
#     The MIME media type of the document, with optional parameters.
#  Body:
#     The actual content of the error document is included in the type-map
#     file using the Body header. This header must contain a string that
#     designates a delimiter for the body content. Then all following lines
#     in the type map file will be considered part of the document body until
#     the delimiter string is found. 
# Fileserv will select the error description whose Content-language header
# matches the language definitions from the Accept-Language request header.
#error-dir /var/lib/fileserv/error

# INDEX TEMPLATE
#
# The following statements control generation of directory index
# listings for directories, where these are allowed (see the 'listing'
# statement, above).

# By default, fileserv uses the built-in template (see src/defidx.html).
# The index-template statement supplies alternative template file. 
#index-template /var/lib/fileserv/index.tmpl

# Name of the CSS file. It will be available in the $INDEXCSS template
# variable.
#index-css /css/autoindex.css

# Define icons for various MIME types.
# Syntax:
#    mime-icon URL alt=TEXT TYPELIST
# where URL is the URL of the image file to be displayed for matching files,
#       TEXT is the alternative text,
#       TYPELIST is a whitespace-delimited list of one or more globbing
#         patterns.
# When a file is being processed for the listing, its MIME type is determined
# using rules from the mime-types-file, then defined mime-icons are scanned
# until the MIME type matches one of patterns in the TYPELIST.

# mime-icon /icons/compressed.gif alt=CMP */x-compress */x-gzip */x-bzip2
# mime-icon /icons/text.gif alt=TXT text/*
# mime-icon /icons/image2.gif alt=IMG image/*
# mime-icon /icons/sound2.gif alt=SND audio/*
# mime-icon /icons/movie.gif alt=VID video/*
# mime-icon /icons/text.gif alt=TXT text/*
# mime-icon /icons/image2.gif alt=IMG image/*
# mime-icon /icons/sound2.gif alt=SND audio/*
# mime-icon /icons/movie.gif alt=VID video/*
# mime-icon /icons/unknown.gif alt=FILE */*

# Define icons for various file names.
# Syntax:
#    name-icon URL alt=TEXT SUFFIXLIST
# where URL is the URL of the image file to be displayed for matching files,
#       TEXT is the alternative text,
#       SUFFIXLIST is a whitespace-delimited list of one or more file suffixes.
#
# name-icon /icons/back.gif alt=BACK ..
# name-icon /icons/script.gif alt=SCRIPT .conf .sh .shar .csh .ksh .tcl
# name-icon /icons/hand.right.gif README
# name-icon /icons/compressed.gif alt=CMP .Z .z .tgz .txz .tbz .gz .xz .bz2 .lzip
# .zip
# name-icon /icons/uuencoded.gif alt=UU .uu
# name-icon /icons/a.gif alt=PS .ps .ai .eps
# name-icon /icons/layout.gif alt=HTML .html .shtml .htm
# name-icon /icons/layout.gif alt=PDF .pdf
# name-icon /icons/text.gif alt=TXT .txt
# name-icon /icons/c.gif alt=SRC .c
# name-icon /icons/p.gif alt=SRC .pl .py
# name-icon /icons/f.gif alt=SRC .for
# name-icon /icons/dvi.gif alt=DVI .dvi
# name-icon /icons/tex.gif alt=TEX .tex
# name-icon /icons/bomb.gif alt=CORE core

# Define icons for various file types.
# Syntax:
#    type-icon URL alt=TEXT TYPELIST
# where URL is the URL of the image file to be displayed for matching files,
#       TEXT is the alternative text,
#       TYPELIST is a whitespace-delimited list of one or more file types.
# 
# type-icon /icons/folder.gif alt=DIR DIRECTORY
# type-icon /icons/blank.gif alt=ICO BLANK
# type-icon /icons/unknown.gif alt=FILE FILE
