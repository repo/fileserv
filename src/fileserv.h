/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <regex.h>
#include <sys/queue.h>
#define MHD_PLATFORM_H
#include <microhttpd.h>

#ifndef SIZE_T_MAX
# define SIZE_T_MAX ((size_t)-1)
#endif

#if MHD_VERSION < 0x00097002
# define MHD_HANDLER_RETTYPE int
#else
# define MHD_HANDLER_RETTYPE enum MHD_Result
#endif

extern char const *progname;
extern char *config_file;
extern char *pidfile;
extern char *index_css;
extern char *tmpdir;
extern char *forwarded_header;
extern char *mime_types_file;
extern char *user;
extern char *group;
extern char *address;
extern char *index_css;
extern int verbose;
extern char *error_dir;

enum fileserv_error {
	FSE_SUCCESS,  /* Processing successful */
	FSE_SYNTAX,   /* Syntax error encountered */
	FSE_ACCESS,   /* Access denied */
	FSE_ERROR     /* System error encountered */
};

void error(char const *fmt, ...);
void info(char const *fmt, ...);
void syslog_enable(void);
int set_log_facility(char const *arg);

void fileserv_logger(void *arg, const char *fmt, va_list ap);

void alloc_warn(void);
void *nrealloc(void *ptr, size_t nmemb, size_t size);
void *n2realloc(void *p, size_t *pn, size_t s);

void xmalloc_fail(void);
void *xmalloc(size_t s);
void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *ptr, size_t size);
void *xnrealloc(void *ptr, size_t nmemb, size_t size);
char *xstrdup(char const *str);

void runas(char const *runas_user, char const *runas_group);

#if HAVE_LIBWRAP
MHD_HANDLER_RETTYPE fileserv_acl(void *cls, const struct sockaddr *addr, socklen_t addrlen);
#else
# define fileserv_acl NULL
#endif

void pidfile_remove(void);
void pidfile_create(void);
void pidfile_check(void);

void urimap_add(char *arg);

typedef struct fileserv_config {
	int follow:1;            /* Follow symbolic links */
	int listing:1;           /* Show directory listing, if a directory
				    is requested and no index file exists */
	int list_unreadable:1;   /* List unreadable files */
	
	char **index_files_v;    /* Names of index files */
	size_t index_files_c;    /* Number of index files */
	size_t index_files_n;    /* Index of the first allocated index file */
	
	regex_t *hidden_files_rxv;
	size_t hidden_files_rxc;
	size_t hidden_files_rxn;
} CONFIG;

enum {
	CTXNONE = 0,
	CTXGLOB = 0x01,
	CTXDIR  = 0x02
};

void readconfig(void);

int dirconfig(char const *path, size_t prefix_len, CONFIG **);
int config_parse(const char *file, CONFIG *conf, int ctx);
CONFIG *config_init(void);
CONFIG *config_clone(CONFIG const *source);
void config_free(CONFIG *conf);

char *catfile_suf_n(char const *dir, size_t len, char const *file,
		    char const *suf);
char *catfile_suf(char const *dir, char const *file, char const *suf);

char *catfile_n(char const *dir, size_t len, char const *file);
char *catfile(char const *dir, char const *file);

int http_error(struct MHD_Connection *conn, char const *method,
	       char const *url, int status);
void http_log(struct MHD_Connection *conn, char const *method,
	      char const *url, int status, struct stat const *st);

typedef enum {
	ISC_NAME,
	ISC_TIME,
	ISC_SIZE,
	ISC_DESC
} INDEX_SORT_COL;

typedef enum {
	ISO_ASC,
	ISO_DESC
} INDEX_SORT_ORD;

int directory_index(int fd,
		    CONFIG const *conf, char const *uri, char const *path,
		    INDEX_SORT_COL col, INDEX_SORT_ORD ord);

INDEX_SORT_COL index_sort_col_from_arg(int c);
int index_sort_col_to_arg(INDEX_SORT_COL c);
INDEX_SORT_ORD index_sort_ord_from_arg(int c);
int index_sort_ord_to_arg(INDEX_SORT_ORD c);

int filename_is_special(char const *name);
int filename_is_hidden(char const *name, CONFIG const *conf);
int filename_is_valid(char const *name);

int parse_template_string(char const *str);
int parse_template_file(char const *file_name);

void trusted_ip_add(char const *s);
char *get_remote_ip(struct MHD_Connection *conn);

typedef struct icon {
	char *name;
	char *src;
	char *alt;
	TAILQ_ENTRY(icon) link;
} ICON;

ICON const *icon_by_mime(char const *type);
void add_icon_by_mime(char const *type, char const *src, char const *alt);
ICON const *icon_by_name(char const *name);
void add_icon_by_name(char const *name, char const *src, char const *alt);
ICON const *icon_by_type(char const *type);
void add_icon_by_type(char const *name, char const *src, char const *alt);

typedef struct lang_variant {
	char *lang;
	off_t header_start;
	off_t body_start;
	size_t body_size;
	STAILQ_ENTRY(lang_variant) next;
} LANG_VAR;

typedef	STAILQ_HEAD(, lang_variant) LANG_VAR_LIST;

void lang_var_list_free(LANG_VAR_LIST *lst);
LANG_VAR *select_language(LANG_VAR_LIST *lst, char const *acc);
