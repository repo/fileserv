/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */

#include "fileserv.h"
#include <string.h>
#include <errno.h>

void *
nrealloc(void *ptr, size_t nmemb, size_t size)
{
	if (size < SIZE_T_MAX / nmemb)
		ptr = realloc(ptr, nmemb * size);
	else
		ptr = NULL;
	return ptr;
}

void *
n2realloc(void *p, size_t *pn, size_t s)
{
	size_t n = *pn;

	if (!p) {
		if (n == 0) {
			/* The approximate size to use for initial small
			   allocation requests, when the invoking code
			   specifies an old size of zero. 64 bytes is the
			   largest "small" request for the GNU C library
			   malloc.  */
			enum { DEFAULT_MXFAST = 64 };

			n = DEFAULT_MXFAST / s;
			n += !n;
		}
	} else {
		/* Set N = ceil (1.5 * N) so that progress is made if N == 1.
		   Check for overflow, so that N * S stays in size_t range.
		   The check is slightly conservative, but an exact check isn't
		   worth the trouble.  */
		if ((size_t) -1 / 3 * 2 / s <= n)
			return NULL;
		n += (n + 1) / 2;
	}
	p = realloc(p, n * s);
	if (p)
		*pn = n;
	return p;
}

void
alloc_warn(void)
{
	int ec = errno;
	error("not enough memory");
	errno = ec;
}

void
xmalloc_fail(void)
{
	error("not enough memory");
	exit(1);
}

void *
xmalloc(size_t s)
{
	void *p = malloc(s);
	if (!p)
		xmalloc_fail();
	return p;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	char *ptr;
	if (size < SIZE_T_MAX / nmemb)
		ptr = calloc(nmemb, size);
	else
		ptr = NULL;
	if (!ptr)
		xmalloc_fail();
	return ptr;
}

void *
xnrealloc(void *ptr, size_t nmemb, size_t size)
{
	ptr = nrealloc(ptr, nmemb, size);
	if (!ptr)
		xmalloc_fail();
	return ptr;
}

void *
xrealloc(void *ptr, size_t size)
{
	return xnrealloc(ptr, 1, size);
}

char *
xstrdup(char const *str)
{
	char *ptr = strdup(str);
	if (!ptr)
		xmalloc_fail();
	return ptr;
}

