/* This file is part of fileserv.
   Copyright (C) 2017-2024 Sergey Poznyakoff

   Fileserv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Fileserv is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with fileserv.  If not, see <http://www.gnu.org/licenses/>. */
#include <tcpd.h>
#include "fileserv.h"

MHD_HANDLER_RETTYPE
fileserv_acl(void *cls, const struct sockaddr *addr,  socklen_t addrlen)
{
	struct request_info req;
	request_init(&req,
		     RQ_DAEMON, "fileserv",
		     RQ_CLIENT_SIN, addr,
		     RQ_SERVER_SIN, cls,
		     NULL);
	sock_methods(&req);
	return hosts_access(&req) ? MHD_YES : MHD_NO;
}
