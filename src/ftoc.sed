# This file is part of fileserv
# Copyright (C) 2009-2024 Sergey Poznyakoff
# Distributed under the terms of the GNU General Public License, either
# version 3, or (at your option) any later version. See file COPYING
# for the text of the license.

# Provide leading comment and quote
1i\
/* -*- buffer-read-only: t -*- vi: set ro:\
   THIS FILE IS GENERATED AUTOMATICALLY. PLEASE, DO NOT EDIT */\
"\\

# Provide trailing quote
$a\
"

# Remove empty lines and comments
/ *#/d
/^ *$/d
# Escape quotes and backslashes
s/["\]/\\&/g
# Add newline and continuation character at the end of each line
s/$/\\n\\/
# End

