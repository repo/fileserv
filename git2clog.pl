# Pretty simple ChangeLog generator                  -*-perl-*-
# Copyright (C) 2019-2024 Sergey Poznyakoff
# Distributed under the terms of the GNU General Public License, either
# version 3, or (at your option) any later version. See file COPYING
# for the text of the license.
use strict;
use warnings;

my $name = shift @ARGV || 'ChangeLog';
my $outfile = "$name.$$";

END {
    if (-f $outfile) {
	unlink $outfile
    }
}

my $header;

die "must be run in a cloned source tree\n" unless -d '.git';

if (-f $name) {
    open(FH, '<', $name)
	or die "can't open $name for reading: $!\n";
    chomp($header = <FH>);
    close FH
}

open(STDIN, '-|',
     q{git log --pretty='format:<%ci>%an  <%ae>%n%n%s%n%n%b%n'})
    or die "can't run git: $!\n";

open(STDOUT, '>', $outfile)
    or die "can't open temporary file\n";

my $nl = 0;
while (<>) {
    chomp;
    if (/^\s*$/) {
	$nl++;
    } else {
	if ($nl) {
	    print "\n";
	    $nl = 0;
        }
	if (s/^<([\d-]+) .*?>(.*)/$1 $2/) {
	    if (defined($header)) {
		if ($_ eq $header) {
		    exit(0)
		}
		$header = undef
	    }
        } else {
	    print "\t";
	}
	print "$_\n";
    }
}

print "\f\nLocal Variables:\n";
print <<'EOT';
mode: change-log
version-control: never
buffer-read-only: t
End:
EOT
;

close STDOUT;
if (-f $name) {
    unlink $name or die "can't unlink $name: $!\n";
}
rename $outfile, $name or die "can't rename $outfile to $name: $!\n";


    

